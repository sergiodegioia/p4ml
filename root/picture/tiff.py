#!python
# -*- coding: utf-8 -*-
"""
This is a wrapper library meant not to link presentation logic
contained in presentation package with the image processing technology
Created on Mon Sep  4 00:52:17 2023

@author: sdegi
"""
from skimage import io
from pathlib import Path

def load( image: Path):
    return io.MultiImage( image.__str__())[ 0]

if __name__ == '__main__':
    print(f'{__doc__}')