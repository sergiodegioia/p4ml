# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button 
import matplotlib.animation as animation
from matplotlib.backend_bases import MouseButton

class Dashboard:
    """Display a running sequence of images

        Parameters
        ----------
        frames: np.ndarray
            Array of image data with shape (N, x, y) N number of frames, row and column sizes.

        pause_millisec: int
            Refresh time in milliseconds between two consecutive images.

    """

    def __init__(self, frames: np.ndarray, refresh_time_millisec: int) -> None:

        # INITIALIZE MATPLOTLIB
        fig, self.ax = plt.subplots( 1, 1, figsize=( 6, 6))
        plt.subplots_adjust(bottom=0.2) # <-- make some room for user controls

        # PREPARE ANIMATION
        self.ani = animation.FuncAnimation(
            fig,
            lambda frame: self.update_image( frame),  # method signature changed into refresh callback one
            frames=frames,
            interval=refresh_time_millisec,
            blit = True
        )

        # PLAY AND PAUSE BUTTON POSITIONING
        self.pause_btn = Button( plt.axes([.85, .05, .1, .1]), 'PAUSE') # button axes = [x, y, x_side, y_side]
        self.play_btn = Button( plt.axes([.75, .05, .1, .1]), 'PLAY')
        self.play_btn.set_active( False)
        
        # PLAY AND PAUSE BUTTON: ATTACHING EVENT HANDLERS
        self.pause_btn.on_clicked(
            lambda evt: self.pause_btn_onclick_handler( evt) # method signature changed into on_clicked callback one
        )
        self.play_btn.on_clicked(
            lambda x: self.play_btn_onclick_handler()
        )
        
        # START EVENT LOOP
        plt.show() # <--- this is a blocking instruction, nothing gets executed after it! 
        
    def pause_btn_onclick_handler( self, evt):
        """callback for on_clicked event from the PAUSE Button"""
        self.ani.pause()
        self.play_btn.set_active( True)
        self.pause_btn.set_active( False)
        
    def play_btn_onclick_handler( self):
        """callback for on_clicked event from the PLAY Button"""
        self.ani.resume()
        self.play_btn.set_active( False)
        self.pause_btn.set_active( True)
        
    def update_image( self, frame):
        """callback for screen update"""
        self.ax.clear()
        return (self.ax.imshow( frame),)
        

