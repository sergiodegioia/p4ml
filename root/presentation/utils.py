import yaml
import warnings
import logging

class Config:
    """Its fields are application's condfiguration values read from config_file
        if found at instantiation time, or default values.
    
        No methods are provided (other than __repr__) because its main purpose is to speed up
        the developer who makes use of a smart ide to have config keys suggested by it.
        Moreover it allows the application to run even though config file does not have all
        needed keys or even if it is not found, with appropriate user warning.
    """
    def __init__( self, config_filename) -> None:
        self.config_filename = config_filename
        self.multipage_image_absolute_filename = 'C:\\Users\\sdegi\\home\\pers\\fisica\\ricerca\\Olomouc\\data\\OneArmOnly\\20230330\\1a.tiff'
        self.refresh_time_in_millis = 100

        # LOAD CONFIGURATION
        try:
            with open( config_filename, 'r') as f:
                try:
                    config = yaml.full_load( f)
                except yaml.constructor.ConstructorError as ce:
                    warnings.warn( f'Configuration file not loaded for the following reason: {ce}'
                    f'\nThe following default configuration values will be used: {self}')
                    return
        except FileNotFoundError as fne:
            warnings.warn( f'Configuration file "{config_filename}" not found.'
                f'\nThe following default configuration values will be used: {self}')
            return

        key = 'multipage_image_absolute_filename';
        if key in config:
            self.multipage_image_absolute_filename = config[ key]
        else:
            warnings.warn( f'Value not found in configuration file for {key}.'
                f'The default value will be used "{key}={self.multipage_image_absolute_filename}".')

        key = 'refresh_time_in_millis';
        if key in config:
            self.refresh_time_in_millis = config['refresh_time_in_millis']
        else:
            warnings.warn( f'Value not found in configuration file for {key}.'
                f'The default value will be used "{key}={self.refresh_time_in_millis}".')

        logging.info(f'configuration loaded: {self}')


    def __repr__(self) -> str:
        return f'''{self.__class__.__name__}(
            multipage_image_absolute_filename = {self.multipage_image_absolute_filename},
            refresh_time_in_millis = {self.refresh_time_in_millis},
        )
        '''
        
