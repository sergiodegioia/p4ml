#!python
# -*- coding: utf-8 -*-
from root.picture import tiff
from pathlib import Path
from root.presentation.dashboard import Dashboard
from root.presentation.utils import Config
import logging

def show_images( config_filename = 'config.yaml'):
    """application main function"""

    # LOAD CONFIGURATION
    config = Config( config_filename)

    imagefile = Path( config.multipage_image_absolute_filename)

    # LOAD MULTIPAGE-IMAGE FILE FROM A CUSTOM LIBRARY
    data = tiff.load( imagefile)

    # DISPLAY USER-STOPPABLE RUNNING SEQUENCE OF PICTURES FROM THE MULTIPAGE IMAGE FIILE
    Dashboard( data, config.refresh_time_in_millis)

if __name__ == '__main__':
    """"
    Synopsys:
        root/presentation/main.py [config_file] [options]

    options:
        --log=log_level: log_level can be one of INFO, DEBUG, WARN, ERROR

    i.e.    
        $ PYTHONPATH=. root/presentation/main.py config.yaml --log=INFO
    """
    def set_logging( arg):
        if arg.startswith( '--log'):
            _, log_level = arg.split( '=')
            numeric_level = getattr( logging, log_level.upper())
            logging.basicConfig( level=numeric_level)

    import sys
    if len( sys.argv) > 1:
        first_arg = sys.argv[ 1]
        if not first_arg.startswith('-'):
            if len( sys.argv) > 2:
                second_arg = sys.argv[ 2]
                set_logging( second_arg)
            show_images( config_filename = first_arg)
        else:
            set_logging( first_arg)
            show_images()
    else:
        if len( sys.argv) > 2:
            second_arg = sys.argv[ 2]
            set_logging( second_arg)
            show_images()
        show_images()



